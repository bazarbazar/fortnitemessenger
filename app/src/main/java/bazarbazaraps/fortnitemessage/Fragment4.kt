package bazarbazaraps.fortnitemessage


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import bazarbazaraps.fortnitemessage.R
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Fragment4 : Fragment() {

    private lateinit var listView: GridView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment






        //  val sample_names_const = arrayOf(

        //          R.drawable.baller,
        //          R.drawable.bestmates,
        //          R.drawable.boneless,
        //          R.drawable.breakin,
        //          R.drawable.chicken,
        //          R.drawable.discofever,
        //          R.drawable.electroshuffle,
        //          R.drawable.flippinsexy,
        //          R.drawable.freestylin,
        //          R.drawable.fresh,
        //          R.drawable.groovejam,
        //          R.drawable.hype,
        //          R.drawable.makeitrain,
        //          R.drawable.orangejustice,
        //          R.drawable.popcorn,
        //          R.drawable.reanimated,
        //          R.drawable.rocketrodeo,
        //          R.drawable.rockout,
        //          R.drawable.starpower,
        //          R.drawable.takethel,
        //          R.drawable.therobot,
        //          R.drawable.tidy,
        //          R.drawable.wiggle,
        //          R.drawable.zany   )

        val  sample_names_const = ArrayList<Int>()


        sample_names_const.add(R.drawable.weapons1)
        sample_names_const.add(R.drawable.weapons2)
        sample_names_const.add(R.drawable.weapons3)
        sample_names_const.add(R.drawable.weapons4)
        sample_names_const.add(R.drawable.weapons5)
        sample_names_const.add(R.drawable.weapons6)
        sample_names_const.add(R.drawable.weapons7)
        sample_names_const.add(R.drawable.weapons8)
        sample_names_const.add(R.drawable.weapons9)
        sample_names_const.add(R.drawable.weapons10)
        sample_names_const.add(R.drawable.weapons11)
        sample_names_const.add(R.drawable.weapons12)
        sample_names_const.add(R.drawable.weapons13)
        sample_names_const.add(R.drawable.weapons14)
        sample_names_const.add(R.drawable.weapons15)
        sample_names_const.add(R.drawable.weapons16)
        sample_names_const.add(R.drawable.weapons17)
        sample_names_const.add(R.drawable.weapons18)
        sample_names_const.add(R.drawable.weapons19)
        sample_names_const.add(R.drawable.weapons20)
        sample_names_const.add(R.drawable.weapons21)
        sample_names_const.add(R.drawable.weapons22)
        sample_names_const.add(R.drawable.weapons23)
        sample_names_const.add(R.drawable.weapons24)
        sample_names_const.add(R.drawable.weapons25)
        sample_names_const.add(R.drawable.weapons26)
        sample_names_const.add(R.drawable.weapons27)
        sample_names_const.add(R.drawable.weapons28)
        sample_names_const.add(R.drawable.weapons29)
        sample_names_const.add(R.drawable.weapons30)
        sample_names_const.add(R.drawable.weapons31)
        sample_names_const.add(R.drawable.weapons32)
        sample_names_const.add(R.drawable.weapons33)
        sample_names_const.add(R.drawable.weapons34)

        val sample_names_const_names = arrayOf<String>(
                "weapons1",
                "weapons2",
                "weapons3",
                "weapons4",
                "weapons5",
                "weapons6",
                "weapons7",
                "weapons8",
                "weapons9",
                "weapons10",
                "weapons11",
                "weapons12",
                "weapons13",
                "weapons14",
                "weapons15",
                "weapons16",
                "weapons17",
                "weapons18",
                "weapons19",
                "weapons20",
                "weapons21",
                "weapons22",
                "weapons23",
                "weapons24",
                "weapons25",
                "weapons26",
                "weapons27",
                "weapons28",
                "weapons29",
                "weapons30",
                "weapons31",
                "weapons32",
                "weapons33",
                "weapons34")



        val rootView = inflater.inflate(R.layout.fragment4, container, false)

        val adapter = MySimpleArrayAdapter(context, sample_names_const)

        listView = rootView.findViewById(R.id.gridview4)
        listView.adapter = adapter


        //val mediaPlayer = MediaPlayerSingleton.instance
        //mediaPlayer.mp = MediaPlayer.create(context, MP_instances[0])


        fun share_audio( file_path: String ){
            val audio =  Uri.parse(file_path)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "image/png"
            shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

            startActivity(Intent.createChooser(shareIntent, "FORTNITE share..."))

        }

        listView.setOnItemClickListener {

            parent, view, position, id ->









        }

        listView.setOnItemLongClickListener { parent, view, position, id ->
            share_audio("android.resource://bazarbazaraps.fortnitemessage/drawable/" + sample_names_const_names[position])
            true
        }

        return rootView

    }


}
