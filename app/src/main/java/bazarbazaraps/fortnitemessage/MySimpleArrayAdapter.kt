package bazarbazaraps.fortnitemessage

import android.content.Context
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import bazarbazaraps.fortnitemessage.R

class MySimpleArrayAdapter(context: Context?, private val values: ArrayList<Int>) : ArrayAdapter<Int>(context, -1, values) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.row_main, parent, false)
        val imageView = rowView.findViewById(R.id.imageView1) as ImageView


    // change the icon for Windows and iPhone

    imageView.setImageResource(values[position])



        return rowView
    }


}
