package bazarbazaraps.fortnitemessage
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                Fragment1()
            }
            1 -> Fragment2()
            2 -> Fragment3()
            3 -> Fragment4()

            else-> {return Fragment5()

            }
        }
    }

    override fun getCount(): Int {
        return 5
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "MUSIC"
            1 -> "STICKERS"
            2 -> "ICONS"
            3 -> "WEAPONS"

            else -> {
                return "OUTFITS"
            }
        }
    }


}