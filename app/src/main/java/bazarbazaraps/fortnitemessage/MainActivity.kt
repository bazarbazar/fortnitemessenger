package bazarbazaraps.fortnitemessage

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import bazarbazaraps.fortnitemessage.R.id.tabs_main
import bazarbazaraps.fortnitemessage.R.id.viewpager_main

import kotlinx.android.synthetic.main.activity_main.*

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd


class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



       val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewpager_main.offscreenPageLimit = 4
        viewpager_main.adapter = fragmentAdapter
        //viewpager_main.setPageTransformer(true, CubeOutTransformer())
        tabs_main.setupWithViewPager(viewpager_main)

        tabs_main.getTabAt(0)!!.setIcon(R.drawable.music_ic)
        tabs_main.getTabAt(1)!!.setIcon(R.drawable.sticker_ic)
        tabs_main.getTabAt(2)!!.setIcon(R.drawable.picture_ic)
        tabs_main.getTabAt(3)!!.setIcon(R.drawable.weap_ic)
        tabs_main.getTabAt(4)!!.setIcon(R.drawable.outfit_ic)




        fun Context.toast(message: CharSequence) =
                Toast.makeText(this, message, Toast.LENGTH_LONG).show()


        this.toast("LONG PUSH ON THE ITEM FOR SHARE")

/*        val sample_names_const = arrayListOf<String>(
                "argentinagoal1",
               "argentinagoal2",
               "argentinagoal3",
               "argentinagoal4",
               "argentinagoal5"


       )

       val listView = findViewById<ListView>(R.id.samples_list_view)
       listView.adapter = MyCustomAdapter(this)





       val MP_instances = ArrayList<Int>()

       MP_instances.add(R.raw.argentinagoal1)
       MP_instances.add(R.raw.argentinagoal2)
       MP_instances.add(R.raw.argentinagoal3)
       MP_instances.add(R.raw.argentinagoal4)
       MP_instances.add(R.raw.argentinagoal5)




       mp = MediaPlayer.create(this, R.raw.argentinagoal1)
       fun play(sample: Int) {
           mp = MediaPlayer.create(this, sample)

           mp.start()
           mp.isLooping = false
       }

       fun reset_sound(sample: Int) {

           if (mp.isPlaying) {
               mp.reset();
               mp = MediaPlayer.create(this, sample)
           }


       }

       fun share_audio(file_path: String) {
           val audio = Uri.parse(file_path)

           val shareIntent = Intent()
           shareIntent.action = Intent.ACTION_SEND
           shareIntent.type = "audio/mpeg"
           shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
           shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

           startActivity(Intent.createChooser(shareIntent, "Rick Morty SB share..."))

       }


       listView.setOnItemClickListener { parent, view, position, id ->
           reset_sound(MP_instances[position])
           play(MP_instances[position])
                mInterstitialAd.show()


       }

       listView.setOnItemLongClickListener { parent, view, position, id ->
           share_audio("android.resource://bazarbazaraps.testtabview/raw/" + sample_names_const[position])

/*
              saveas()
              Toast.makeText(this, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString(), Toast.LENGTH_LONG).show()
              */
           true
       }

   }
}

private class MyCustomAdapter(context: Context) : BaseAdapter() {


   private val sample_names = arrayListOf<String>(
           "Argentina Goal 1",
           "Argentina Goal 2",
           "Argentina Goal 3",
           "Argentina Goal 4",
           "Argentina Goal 5"

   )

   private var mContext = context

   init {
       mContext = context
   }

   override fun getCount(): Int {
       return sample_names.size
   }

   override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
       //val textView = TextView(mContext)
       //textView.text="row"
       // return textView

       val layoutInflater = LayoutInflater.from(mContext)
       val rowMain = layoutInflater.inflate(R.layout.row_main, parent, false)
       val sampleTextView = rowMain.findViewById<TextView>(R.id.sampletextView)
       sampleTextView.text = sample_names[position]
       return rowMain
   }

   override fun getItemId(position: Int): Long {
       return position.toLong()
   }

   override fun getItem(position: Int): Any {
       return "test"
   }
*/

   }
}