package bazarbazaraps.fortnitemessage


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.widget.*
import bazarbazaraps.fortnitemessage.R
import bazarbazaraps.fortnitemessage.R.id.imageView1

import com.google.android.gms.common.util.Strings

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Fragment1 : Fragment() {


    private lateinit var listView: GridView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment






     //  val sample_names_const = arrayOf(

     //          R.drawable.baller,
     //          R.drawable.bestmates,
     //          R.drawable.boneless,
     //          R.drawable.breakin,
     //          R.drawable.chicken,
     //          R.drawable.discofever,
     //          R.drawable.electroshuffle,
     //          R.drawable.flippinsexy,
     //          R.drawable.freestylin,
     //          R.drawable.fresh,
     //          R.drawable.groovejam,
     //          R.drawable.hype,
     //          R.drawable.makeitrain,
     //          R.drawable.orangejustice,
     //          R.drawable.popcorn,
     //          R.drawable.reanimated,
     //          R.drawable.rocketrodeo,
     //          R.drawable.rockout,
     //          R.drawable.starpower,
     //          R.drawable.takethel,
     //          R.drawable.therobot,
     //          R.drawable.tidy,
     //          R.drawable.wiggle,
     //          R.drawable.zany   )

        val  sample_names_const = ArrayList<Int>()

        sample_names_const.add(R.drawable.baller)
        sample_names_const.add(R.drawable.bestmates)
        sample_names_const.add(R.drawable.boneless)
        sample_names_const.add(R.drawable.breakin)
        sample_names_const.add(R.drawable.chicken)
        sample_names_const.add(R.drawable.discofever)
        sample_names_const.add(R.drawable.electroshuffle)
        sample_names_const.add(R.drawable.flippinsexy)
        sample_names_const.add(R.drawable.freestylin)
        sample_names_const.add(R.drawable.fresh)
        sample_names_const.add(R.drawable.groovejam)
        sample_names_const.add(R.drawable.hype)
        sample_names_const.add(R.drawable.makeitrain)
        sample_names_const.add(R.drawable.orangejustice)
        sample_names_const.add(R.drawable.popcorn)
        sample_names_const.add(R.drawable.reanimated)
        sample_names_const.add(R.drawable.rocketrodeo)
        sample_names_const.add(R.drawable.rockout)
        sample_names_const.add(R.drawable.starpower)
        sample_names_const.add(R.drawable.takethel)
        sample_names_const.add(R.drawable.therobot)
        sample_names_const.add(R.drawable.tidy)
        sample_names_const.add(R.drawable.wiggle)
        sample_names_const.add(R.drawable.zany)

        val sample_names_const_names = arrayOf<String>(
               "baller",
               "bestmates",
               "boneless",
               "breakin",
               "chicken",
               "discofever",
               "electroshuffle",
               "flippinsex",
               "freestylin",
               "fresh",
               "groovejam",
               "hype",
               "makeitrain",
               "orangejustice",
               "popcorn",
               "reanimated",
               "rocketrode",
               "rockout",
               "starpower",
               "takethel",
               "therobot",
               "tidy",
               "wiggle",
               "zany")

        val  MP_instances = ArrayList<Int>()

        MP_instances.add(R.raw.baller)
        MP_instances.add(R.raw.bestmates)
        MP_instances.add(R.raw.boneless)
        MP_instances.add(R.raw.breakin)
        MP_instances.add(R.raw.chicken)
        MP_instances.add(R.raw.discofever)
        MP_instances.add(R.raw.electroshuffle)
        MP_instances.add(R.raw.flippinsexy)
        MP_instances.add(R.raw.freestylin)
        MP_instances.add(R.raw.fresh)
        MP_instances.add(R.raw.groovejam)
        MP_instances.add(R.raw.hype)
        MP_instances.add(R.raw.makeitrain)
        MP_instances.add(R.raw.orangejustice)
        MP_instances.add(R.raw.popcorn)
        MP_instances.add(R.raw.reanimated)
        MP_instances.add(R.raw.rocketrodeo)
        MP_instances.add(R.raw.rockout)
        MP_instances.add(R.raw.starpower)
        MP_instances.add(R.raw.takethel)
        MP_instances.add(R.raw.therobot)
        MP_instances.add(R.raw.tidy)
        MP_instances.add(R.raw.wiggle)
        MP_instances.add(R.raw.zany)

        val rootView = inflater.inflate(R.layout.fragment_first, container, false)

        val adapter = MySimpleArrayAdapter(context, sample_names_const)

        listView = rootView.findViewById(R.id.gridview1)
        listView.adapter = adapter


        val mediaPlayer = MediaPlayerSingleton.instance
       mediaPlayer.mp = MediaPlayer.create(context, MP_instances[0])


        fun share_audio( file_path: String ){
            val audio =  Uri.parse(file_path)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "audio/mpeg"
            shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

            startActivity(Intent.createChooser(shareIntent, "FORTNITE share..."))

        }

        listView.setOnItemClickListener {

            parent, view, position, id ->
            mediaPlayer.reset_sound(MP_instances[position],context)
            // reset_sound(mediaPlayer, MP_instances[position])
            mediaPlayer.MPplay( MP_instances[position],context)








        }

        listView.setOnItemLongClickListener { parent, view, position, id ->
            share_audio("android.resource://bazarbazaraps.fortnitemessage/raw/" + sample_names_const_names[position])
            true
        }

        return rootView

    }


}