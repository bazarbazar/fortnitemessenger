package bazarbazaraps.fortnitemessage


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import bazarbazaraps.fortnitemessage.R



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Fragment2 : Fragment() {

    private lateinit var listView: GridView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment






        //  val sample_names_const = arrayOf(

        //          R.drawable.baller,
        //          R.drawable.bestmates,
        //          R.drawable.boneless,
        //          R.drawable.breakin,
        //          R.drawable.chicken,
        //          R.drawable.discofever,
        //          R.drawable.electroshuffle,
        //          R.drawable.flippinsexy,
        //          R.drawable.freestylin,
        //          R.drawable.fresh,
        //          R.drawable.groovejam,
        //          R.drawable.hype,
        //          R.drawable.makeitrain,
        //          R.drawable.orangejustice,
        //          R.drawable.popcorn,
        //          R.drawable.reanimated,
        //          R.drawable.rocketrodeo,
        //          R.drawable.rockout,
        //          R.drawable.starpower,
        //          R.drawable.takethel,
        //          R.drawable.therobot,
        //          R.drawable.tidy,
        //          R.drawable.wiggle,
        //          R.drawable.zany   )

        val  sample_names_const = ArrayList<Int>()


        sample_names_const.add(R.drawable.sticker1)
        sample_names_const.add(R.drawable.sticker2)
        sample_names_const.add(R.drawable.sticker3)
        sample_names_const.add(R.drawable.sticker4)
        sample_names_const.add(R.drawable.sticker5)
        sample_names_const.add(R.drawable.sticker6)
        sample_names_const.add(R.drawable.sticker7)
        sample_names_const.add(R.drawable.sticker8)
        sample_names_const.add(R.drawable.sticker9)
        sample_names_const.add(R.drawable.sticker10)
        sample_names_const.add(R.drawable.sticker11)
        sample_names_const.add(R.drawable.sticker12)
        sample_names_const.add(R.drawable.sticker13)
        sample_names_const.add(R.drawable.sticker14)
        sample_names_const.add(R.drawable.sticker15)
        sample_names_const.add(R.drawable.sticker16)
        sample_names_const.add(R.drawable.sticker17)
        sample_names_const.add(R.drawable.sticker18)
        sample_names_const.add(R.drawable.sticker19)
        sample_names_const.add(R.drawable.sticker20)
        sample_names_const.add(R.drawable.sticker21)
        sample_names_const.add(R.drawable.sticker22)
        sample_names_const.add(R.drawable.sticker23)
        sample_names_const.add(R.drawable.sticker24)
        sample_names_const.add(R.drawable.sticker25)
        sample_names_const.add(R.drawable.sticker26)
        sample_names_const.add(R.drawable.sticker27)
        sample_names_const.add(R.drawable.sticker28)
        sample_names_const.add(R.drawable.sticker29)
        sample_names_const.add(R.drawable.sticker30)
        sample_names_const.add(R.drawable.sticker31)
        sample_names_const.add(R.drawable.sticker32)
        sample_names_const.add(R.drawable.sticker33)
        sample_names_const.add(R.drawable.sticker34)
        sample_names_const.add(R.drawable.sticker35)
        sample_names_const.add(R.drawable.sticker36)
        sample_names_const.add(R.drawable.sticker37)
        sample_names_const.add(R.drawable.sticker38)
        sample_names_const.add(R.drawable.sticker39)
        sample_names_const.add(R.drawable.sticker40)
        sample_names_const.add(R.drawable.sticker41)
        sample_names_const.add(R.drawable.sticker42)
        sample_names_const.add(R.drawable.sticker43)
        sample_names_const.add(R.drawable.sticker44)
        sample_names_const.add(R.drawable.sticker45)
        sample_names_const.add(R.drawable.sticker46)
        sample_names_const.add(R.drawable.sticker47)
        sample_names_const.add(R.drawable.sticker48)
        sample_names_const.add(R.drawable.sticker49)
        sample_names_const.add(R.drawable.sticker50)
        sample_names_const.add(R.drawable.sticker51)
        sample_names_const.add(R.drawable.sticker52)

        val sample_names_const_names = arrayOf<String>(
                "sticker1",
                "sticker2",
                "sticker3",
                "sticker4",
                "sticker5",
                "sticker6",
                "sticker7",
                "sticker8",
                "sticker9",
                "sticker10",
                "sticker11",
                "sticker12",
                "sticker13",
                "sticker14",
                "sticker15",
                "sticker16",
                "sticker17",
                "sticker18",
                "sticker19",
                "sticker20",
                "sticker21",
                "sticker22",
                "sticker23",
                "sticker24",
                "sticker25",
                "sticker26",
                "sticker27",
                "sticker28",
                "sticker29",
                "sticker30",
                "sticker31",
                "sticker32",
                "sticker33",
                "sticker34",
                "sticker35",
                "sticker36",
                "sticker37",
                "sticker38",
                "sticker39",
                "sticker40",
                "sticker41",
                "sticker42",
                "sticker43",
                "sticker44",
                "sticker45",
                "sticker46",
                "sticker47",
                "sticker48",
                "sticker49",
                "sticker50",
                "sticker51",
                "sticker52")



        val rootView = inflater.inflate(R.layout.fragment_fragment2, container, false)

        val adapter = MySimpleArrayAdapter(context, sample_names_const)

        listView = rootView.findViewById(R.id.gridview2)
        listView.adapter = adapter


        //val mediaPlayer = MediaPlayerSingleton.instance
        //mediaPlayer.mp = MediaPlayer.create(context, MP_instances[0])


        fun share_audio( file_path: String ){
            val audio =  Uri.parse(file_path)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "image/png"
            shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

            startActivity(Intent.createChooser(shareIntent, "FORTNITE share..."))

        }

        listView.setOnItemClickListener {

            parent, view, position, id ->









        }

        listView.setOnItemLongClickListener { parent, view, position, id ->
            share_audio("android.resource://bazarbazaraps.fortnitemessage/drawable/" + sample_names_const_names[position])
            true
        }

        return rootView

    }


}
