package bazarbazaraps.fortnitemessage


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import bazarbazaraps.fortnitemessage.R


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Fragment3 : Fragment() {

    private lateinit var listView: GridView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment






        //  val sample_names_const = arrayOf(

        //          R.drawable.baller,
        //          R.drawable.bestmates,
        //          R.drawable.boneless,
        //          R.drawable.breakin,
        //          R.drawable.chicken,
        //          R.drawable.discofever,
        //          R.drawable.electroshuffle,
        //          R.drawable.flippinsexy,
        //          R.drawable.freestylin,
        //          R.drawable.fresh,
        //          R.drawable.groovejam,
        //          R.drawable.hype,
        //          R.drawable.makeitrain,
        //          R.drawable.orangejustice,
        //          R.drawable.popcorn,
        //          R.drawable.reanimated,
        //          R.drawable.rocketrodeo,
        //          R.drawable.rockout,
        //          R.drawable.starpower,
        //          R.drawable.takethel,
        //          R.drawable.therobot,
        //          R.drawable.tidy,
        //          R.drawable.wiggle,
        //          R.drawable.zany   )

        val  sample_names_const = ArrayList<Int>()


        sample_names_const.add(R.drawable.icons1)
        sample_names_const.add(R.drawable.icons2)
        sample_names_const.add(R.drawable.icons3)
        sample_names_const.add(R.drawable.icons4)
        sample_names_const.add(R.drawable.icons5)
        sample_names_const.add(R.drawable.icons6)
        sample_names_const.add(R.drawable.icons7)
        sample_names_const.add(R.drawable.icons8)
        sample_names_const.add(R.drawable.icons9)
        sample_names_const.add(R.drawable.icons10)
        sample_names_const.add(R.drawable.icons11)
        sample_names_const.add(R.drawable.icons12)
        sample_names_const.add(R.drawable.icons13)
        sample_names_const.add(R.drawable.icons14)
        sample_names_const.add(R.drawable.icons15)
        sample_names_const.add(R.drawable.icons16)
        sample_names_const.add(R.drawable.icons17)
        sample_names_const.add(R.drawable.icons18)
        sample_names_const.add(R.drawable.icons19)
        sample_names_const.add(R.drawable.icons20)
        sample_names_const.add(R.drawable.icons21)
        sample_names_const.add(R.drawable.icons22)
        sample_names_const.add(R.drawable.icons23)
        sample_names_const.add(R.drawable.icons24)
        sample_names_const.add(R.drawable.icons25)
        sample_names_const.add(R.drawable.icons26)
        sample_names_const.add(R.drawable.icons27)
        sample_names_const.add(R.drawable.icons28)
        sample_names_const.add(R.drawable.icons29)
        sample_names_const.add(R.drawable.icons30)
        sample_names_const.add(R.drawable.icons31)
        sample_names_const.add(R.drawable.icons32)
        sample_names_const.add(R.drawable.icons33)
        sample_names_const.add(R.drawable.icons34)
        sample_names_const.add(R.drawable.icons35)
        sample_names_const.add(R.drawable.icons36)


        val sample_names_const_names = arrayOf<String>(
                "icons1",
                "icons2",
                "icons3",
                "icons4",
                "icons5",
                "icons6",
                "icons7",
                "icons8",
                "icons9",
                "icons10",
                "icons11",
                "icons12",
                "icons13",
                "icons14",
                "icons15",
                "icons16",
                "icons17",
                "icons18",
                "icons19",
                "icons20",
                "icons21",
                "icons22",
                "icons23",
                "icons24",
                "icons25",
                "icons26",
                "icons27",
                "icons28",
                "icons29",
                "icons30",
                "icons31",
                "icons32",
                "icons33",
                "icons34",
                "icons35",
                "icons36" )



        val rootView = inflater.inflate(R.layout.fragment3, container, false)

        val adapter = MySimpleArrayAdapter(context, sample_names_const)

        listView = rootView.findViewById(R.id.gridview3)
        listView.adapter = adapter


        //val mediaPlayer = MediaPlayerSingleton.instance
        //mediaPlayer.mp = MediaPlayer.create(context, MP_instances[0])


        fun share_audio( file_path: String ){
            val audio =  Uri.parse(file_path)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "image/png"
            shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

            startActivity(Intent.createChooser(shareIntent, "FORTNITE share..."))

        }

        listView.setOnItemClickListener {

            parent, view, position, id ->









        }

        listView.setOnItemLongClickListener { parent, view, position, id ->
            share_audio("android.resource://bazarbazaraps.fortnitemessage/drawable/" + sample_names_const_names[position])
            true
        }

        return rootView

    }


}